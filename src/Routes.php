<?php declare(strict_types = 1);

return [
    ['GET', '/', ['Waxframe\Controllers\Homepage', 'show']],
];
