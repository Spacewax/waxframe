<?php declare(strict_types = 1);

namespace Waxframe\Template;

interface Renderer
{
    public function render($template, $data = []) : string;
}
