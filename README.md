# WaxFrame

is a small (micro) PHP Framework designed for building websites in an easy manner.


## Server Requirements
PHP version 7.0.0 or newer is recommended.


## License
Please see the [license](LICENSE).
